import graph as g
import bfs_algo as bfs
import sys
import re
import heuristic as h
import draw as draw

arguments = sys.argv[1:]
arg_count = len(arguments)
if (arg_count == 0):
    a = 0
elif (arg_count == 1):
    fd = open(arguments[0], 'r')
    content = fd.read()
    content = re.sub(r'(?m)#.*\n?', '\n', content).split()
    size = int(content[0])
    initial_state_array = content[1::]
    if (len(initial_state_array) == size * size):       
        initial_state = []
        for elem in initial_state_array:
            initial_state.append(int(elem))
        initial_state = tuple(initial_state)
        zero_index = initial_state.index(0)
    else:
        print("Invalid format")
else:
    print("Usage: python3 main.py [file_name]")


# def strip_size(size, state):
#     mask = []
#     new_state = []
#     final = []
#     for i in range(1, size * size + 1):
#         if not ((i - 1) // size == 0 or (i - 1) % size == 0):
#             new_state.append(state[i - 1])
#             mask.append(i % (size * size))
#     for i in new_state:
#         final.append((mask.index(i) + 1) % ((size - 1) * (size - 1)))
#     return tuple(final)

# while size >= 3:
#     graph = g.Graph(initial_state, size, zero_index)
#     came_from = bfs.gready_search(
#         graph,
#         (initial_state, zero_index),
#         h.manhattan_distance) # return [queue, path, last_state]
#     draw.print_result(graph, came_from)
#     initial_state = strip_size(size, came_from[2])
#     size -= 1
#     zero_index = initial_state.index(0)


graph = g.Graph(initial_state, size, zero_index)
# came_from = bfs.dijkstra_search(graph, (initial_state, zero_index))
came_from = bfs.gready_search(
    graph,
    (initial_state, zero_index),
    h.manhattan_distance)
# came_from = bfs.gready_search(
#     graph,
#     (initial_state, zero_index),
#     h.match_priority)
came_from = bfs.a_star(graph,
    (initial_state, zero_index),
    h.manhattan_distance)
# came_from = bfs.a_star(graph,
#     (initial_state, zero_index),
#     h.match_priority)



