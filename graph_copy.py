from queue import *
import functools 

class Graph:
    def __init__(self, initial_state, size, zero_pos):
        self.layer = 0
        self.state = initial_state
        self.nodes = {}
        self.size = size
        self.len = self.size * self.size
        self.last_index = size * size - 1
        self.zero = zero_pos
        self.final_state = self.__get_final_state()
        self.first_layer = self._get_first_layer()
        self.layer_mask = self._get_layer_mask()
        self.priority_states = self.calc_priority_states()
        self.history = 1

    def get_history(self, mask, state):
        if self.history:
            history = {}
            
            ret = [mask[self.size]]
            mask = self.priority_states[mask][0]
            while mask != None:
                next = self.__swap(mask[-1], state[1], state[0])
                history[next[0]] = state[0]
                state = next
                if self.priority_states[mask]: mask = self.priority_states[mask][0]
                else: mask = None
            self.history = 0

    def current_in_priopity(self, state):
        priority_helper = self.mask_from_state(state)
        if priority_helper in self.priority_states:
            val = self.priority_states[tuple(priority_helper)]
            # self.get_history(priority_helper, state)
            return val[1] - 100
        else: return 0

    def mask_from_state(self, state):

        start = self.size * self.layer

        if state:
            priority_helper = []
            for pos1 in self.layer_mask:
                priority_helper.append(state[0].index(pos1))
            priority_helper.append(state[1])
            return tuple(priority_helper)
        return state[0]

    def calc_priority_states(self):
        came_from = {}
        level = {}
        frontier = PriorityQueue()
        for i in range(self.last_index):
            if (i // self.size == 1 and i % self.size > 0) or (i % self.size == 1 and i // self.size > 0):
                zero_pos = i
                frontier.put((tuple(self.first_layer), i), 0)
                level[(tuple(self.first_layer), i)] = 0
                came_from[self.mask_from_state((tuple(self.first_layer), i))] = (None, 0)

        while not frontier.empty():
            current = frontier.get()
            zero_pos = current[1]
            current_level = level[current] + 1
            if current_level > 20: print(len(came_from)); break
            
            for next in self.neighbors(current):
                next_priority_mask = self.mask_from_state(next)
                if next_priority_mask not in came_from or (came_from[next_priority_mask] and (came_from[next_priority_mask][1] > current_level)):
                    level[next] = current_level
                    frontier.put(next, current_level)
                    came_from[next_priority_mask] = (self.mask_from_state(current), current_level)
        return came_from
            

        
    def check_valid(self, state):
        # return state == self.final_state
        for val in self.layer_mask:
            # print(val, state[0])
            if state.index(val) + 1 != val: return False
        return True
        
    def _get_layer_mask(self):
        layer_mask = []
        for i in range(0, self.len):
            if i // self.size == 0:
                layer_mask.append(i + 1)
            elif i % self.size == 0:
                layer_mask.append(i + 1)
        return tuple(layer_mask)

    def _get_first_layer(self):
        first_layer = []
        for i in range(0, self.len):
            if i // self.size == 0:
                first_layer.append(i + 1)
            elif i % self.size == 0:
                first_layer.append(i + 1)
            else:
                first_layer.append(0)
        return tuple(first_layer)

    def __get_final_state(self):
        state = [i for i in range(1, self.len + 1)]
        state[self.len - 1] = 0
        return tuple(state)

    def __swap(self, to, zero, state):
        l = list(state)
        l[to], l[zero] = l[zero], l[to]
        return (tuple(l), to)

    def draw(self, node):
        i = 0
        while i < len(node):
            print(node[i], end=' ' if ((i + 1) % self.size) else '\n')
            i += 1
        print('')

    def neighbors(self, state):
        zero = state[1]
        neighbors = []
        results = []
        if (zero // self.size != 0):
            results.append(zero - self.size)
        if (zero // self.size != self.size - 1):
            results.append(zero + self.size)
        if (zero % self.size):
            results.append(zero - 1)
        if (zero % self.size != self.size - 1):
            results.append(zero + 1)
        for res in results:
            neighbors.append(self.__swap(res, zero, state[0]))
        self.nodes[state[0]] = neighbors
        return neighbors
    
    # def get_path(self, came_from):
    #     path = []
    #     last = (self.final_state, self.last_index)
    #     while last != None:
    #         path.append(last[1])
    #         last = came_from[last]
    #     print(path)
    #     print("start", self.final_state, came_from[(self.final_state, self.last_index)])




class SquareGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []
    
    def in_bounds(self, id):
        (x, y) = id
        return 0 <= x < self.width and 0 <= y < self.height
    
    def passable(self, id):
        return id not in self.walls
    
    def neighbors(self, id):
        (x, y) = id
        results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
        if (x + y) % 2 == 0: results.reverse() # aesthetics
        results = filter(self.in_bounds, results)
        results = filter(self.passable, results)
        return results

# class GridWithWeights(SquareGrid):
#     def __init__(self, width, height):
#         super().__init__(width, height)
#         self.weights = {}
    
#     def cost(self, from_node, to_node):
#         return self.weights.get(to_node, 1)